//
//  AddMoreInfoVC.swift
//  Lifekeeper
//
//  Created by Max Winkler on 12/10/2016.
//  Copyright © 2016 Pristok. All rights reserved.
//

import UIKit
import Firebase

class AddMoreInfoVC: UIViewController,UITextFieldDelegate {
   
    @IBOutlet weak var namefield: UITextField!
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var saveContinueLbl: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var datePicker: UIDatePicker!

    var ref:FIRDatabaseReference?
    var genderValue:String = ""
    var uid = FIRAuth.auth()?.currentUser?.uid
    var completeDate: String = ""
    
    
    func dateChanged(_ sender: UIDatePicker) {
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        
        if let day = componenets.day, let month = componenets.month, let year = componenets.year  {
            completeDate = "\(String(day)  + "/") \(String(month) + "/") \(String(year))"
            print(completeDate)

        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    
    override func viewDidLoad() {
             super.viewDidLoad()
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)

        self.namefield.delegate = self
   
        
    }

    

    @IBAction func genderSelector(_ sender: Any) {
        
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            genderValue = "Male"
        case 1:
           genderValue = "Female"
        default:
            break
        }
    }
    
    
    // Create saveContinue button and create and populate data to db
    
       @IBAction func saveContinueBtn(_ sender: Any) {
        
        let weightAsDouble: Double? = Double(weight.text!)
        let heightAsDouble: Double? = Double(height.text!)
    
        
        if namefield.text != "" && height.text != "" && weight.text != "" {

        ref = FIRDatabase.database().reference()
            ref?.child("userData").child(uid!).setValue([
                
               
                
                "email" : FIRAuth.auth()?.currentUser?.email as String! as AnyObject,
                                                         "fullName" : namefield.text as AnyObject,
                                                         "height" : heightAsDouble!,
                                                         "weight" : weightAsDouble!, "gender": genderValue as AnyObject,
                                                         "totalDistance": 0 as Int, "totalActivity" : 0 as Int, "totalKcal" : 0 as Int, "totalHours" : 0 as Double,
                                                         "thisWeekDistance": 0 as Int, "thisWeekActivity" : 0 as Int, "thisWeekKcal": 0 as Int, "thisWeekHours": 0 as Int,
                                                         "lastWeekDistance" : 0 as Int, "lastWeekActivity" : 0 as Int, "lastWeekKcal": 0 as Int, "lastWeekHours" : 0 as Int,
                                                         "lang" : "English" as AnyObject, "unit" : "Km" as AnyObject, "bday" : completeDate as AnyObject, "activityList" : "0" as AnyObject,
                                                         "activityCountAsInt" : 0 as Int
                ])
           
            /*
            ref?.child("userData").child(uid!).child("activityList").childByAutoId().setValue(["title" : "" as AnyObject,
                                                                                                                                                    "duration" : "0" as AnyObject,
                                                                                                                                                    "kcal": "0" as AnyObject,
                                                                                                                                                    "route" : "0" as AnyObject,
                                                                                                                                                    "timestampSaved": "0" as AnyObject,
                                                                                                                                                    "slope": "0" as AnyObject,
                                                                                                                                                    "avgSpeed": "0" as AnyObject,
                                                                                                                                                    "maxSpeed": "0" as AnyObject,
                                                                                                                                                    "minSpeed": "0" as AnyObject,
                                                                                                                                                    
                                                                                                                                                    
                

                ])
 */
            
            var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

            var vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "thanks")
            
            self.present(vc, animated: true, completion: nil)

        
        }else{
            print("error")
            let alertController = UIAlertController(title: "Something went wrong!", message:
                "Your details have not been updated", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
}
        
}
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    
    
 }


