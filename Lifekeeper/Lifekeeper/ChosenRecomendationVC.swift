//
//  ChosenRecomendationVC.swift
//  
//
//  Created by Max Winkler on 10/02/2017.
//
//

import UIKit

class ChosenRecomendationVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    var titleString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleLabel.text = self.titleString
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     
    }
    

  
}
