//
//  ContentViewController.swift
//  Lifekeeper
//
//  Created by Max Winkler on 02/03/2017.
//  Copyright © 2017 Pristok. All rights reserved.
//

import UIKit
import Firebase


//Distance/time/kcal
class ContentViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thisWeekData: UILabel!
    @IBOutlet weak var lastWeekData: UILabel!
    @IBOutlet var areaSwipedOutlet: UISwipeGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func areaSwiped(_ sender: Any) {
        print("You have swiped")
        titleLabel.text = "time"
        areaSwipedOutlet.isEnabled = false
    }
}
