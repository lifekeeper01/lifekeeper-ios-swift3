//
//  DistanceDemoVC.swift
//  Lifekeeper
//
//  Created by Apple on 13/3/17.
//  Copyright © 2017 Pristok. All rights reserved.
//

import UIKit
import CoreLocation

class DistanceDemoVC: UIViewController, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager!
    var startLocation:CLLocation!
    var lastLocation: CLLocation!
    var traveledDistance:Double = 0
   
    @IBOutlet weak var distanceLBL: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Ask for permission GPS
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
   

    }//end viewDidLoad()
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            
            print("Access authorixed")
            
            
        }
    }
 
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    
        
        if startLocation == nil {
            startLocation = locations.first
        } else {
            if let lastLocation = locations.last {
                let distance = startLocation.distance(from: lastLocation)
                let lastDistance = lastLocation.distance(from: lastLocation)
                traveledDistance += lastDistance
               
                print( "\(startLocation)")
                print( "\(lastLocation)")
                print("FULL DISTANCE: \(traveledDistance)")
                print("STRAIGHT DISTANCE: \(distance)")
                
                
                //adapt to km
               
                let finalDistance = round(distance) / 1000
                distanceLBL.text = String(finalDistance)
                
              
            }
        }
        lastLocation = locations.last
    }
   
}
