//
//  LoadScreenVC.swift
//  Lifekeeper
//
//  Created by Max Winkler on 20/03/2017.
//  Copyright © 2017 Pristok. All rights reserved.
//

import UIKit

class LoadScreenVC: UIViewController {

    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    
    func update(){
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVc")
        
        self.present(vc, animated: true, completion: nil)
        
    }
    

}

