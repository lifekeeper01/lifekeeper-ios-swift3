//
//  MasterTableVC.swift
//  Lifekeeper
//
//  Created by Max Winkler on 10/10/2016.
//  Copyright © 2016 Pristok. All rights reserved.
//

import UIKit
import Firebase

/*struct activityDataStruct{
    
    let title: String = ""
    let duration: String = ""
    let kcal: String = ""
    let route: String = ""
    let timestampSave: String = ""
    let slope: String = ""
    let avgSpeed: String = ""
    let maxSpeed: String = ""
    let minSpeed: String = ""
    
}
 */

class MasterTableVC: UITableViewController {
    
    var finalActivityData: NSArray = []
    var finalTitle: String = ""
    var finaltimeStamp: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var ref: FIRDatabaseReference!
        let uid = (FIRAuth.auth()?.currentUser!.uid)! as String
        ref = FIRDatabase.database().reference()
        
       
        
        ref.child("userData").child(uid).child("activityList").queryOrderedByKey().observe(FIRDataEventType.childAdded, with: { (snapshot) in
            
             let value = snapshot.value as? NSDictionary
             let title = value?["title"] as? String ?? ""
             let duration = value?["duration"] as? String ?? ""
             let kcal = value?["kcal"] as? String ?? ""
             let route = value?["route"] as? String ?? ""
             let timestampSave = value?["timestampSaved"] as? String ?? ""
             let slope = value?["slope"] as? String ?? ""
             let avgSpeed = value?["avgSpeed"] as? String ?? ""
             let maxSpeed = value?["maxSpeed"] as? String ?? ""
             let minSpeed = value?["minSpeed"] as? String ?? ""
            
            self.finalActivityData = [title, timestampSave]
            self.finalTitle = title
            self.finaltimeStamp = timestampSave
            print(self.finalActivityData)
            print(self.finalTitle)
            print(self.finaltimeStamp)
            
           let activityAmount = ref.child("userData").child(uid).child("activityList")
            print(activityAmount)
            
                     self.tableView.reloadData()
            
        })
    
    }
 
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
        //
     
     
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
      var titleLabel = cell?.viewWithTag(1) as! UILabel
        titleLabel.text = "Hello"
        
        return cell!
        
     
    }
 
}
