//
//  MeVC.swift
//  Lifekeeper
//
//  Created by Max Winkler on 10/10/2016.
//  Copyright © 2016 Pristok. All rights reserved.
//

import UIKit
import Firebase

class MeVC: UIViewController, UIScrollViewDelegate{
    

    //Outlets

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var scrollView1: UIScrollView!
    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet weak var allTimeDistance: UILabel!
    @IBOutlet weak var allTimeTime: UILabel!
    @IBOutlet weak var allTimeKcal: UILabel!

      var featureArray = [Dictionary<String,String> ]()
     var ref: FIRDatabaseReference!
    let uid = (FIRAuth.auth()?.currentUser!.uid)! as String
    var finalActListForLoop = 0
    var finalActListForLoopToAccessFinalDistance = ""

    //Vars and lets
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = FIRDatabase.database().reference()
        
     loadDbData()
        
    }
        func loadDbData(){
            
            /*
            FIRAuth.auth()?.addStateDidChangeListener { auth, user in
                if user != nil {
                    
                }}
 */

            
            ref = FIRDatabase.database().reference()
            ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as? NSDictionary
                let username = value!["fullName"] as? String ?? ""
                self.nameLbl.text = username
                
                let totalDistance = "\(value?["totalDistance"] as! Int)"
                
                let totalKcal = "\(value?["totalKcal"] as! Int)"
                let totalHours = "\(value?["totalHours"] as! Double)"
                
                
                
                
                
                
                //let thisWeekActivity = "\(value?["thisWeekActivity"] as! Int)"
                let thisWeekDistance = "\(totalDistance)"
                let thisWeekKcal = "\(totalKcal)"
                let thisWeekHours = "\(totalHours)"
                
                let lastWeekDistance = "\(value?["lastWeekDistance"] as! Int)"
                let lastWeekActivity = "\(value?["lastWeekActivity"] as! Int)"
                let lastWeekKcal = "\(value?["lastWeekKcal"] as! Int)"
                let lastWeekHours = "\(value?["lastWeekHours"] as! Int)"
                
                self.allTimeDistance.text = String(describing: totalDistance)
                self.allTimeTime.text = String(describing: totalHours)
                self.allTimeKcal.text = String(describing: totalKcal)
                
                
                //var pageViewController: UIPageViewController!
                //var pageTitles: NSArray!
                
                let kcalFeature1 = ["title" : "Kilocalories", "thisWeek" : thisWeekKcal, "lastWeek": lastWeekKcal, "image": "fire"    ] as [String : Any]
                let distanceFeature2 = ["title" : "Distance", "thisWeek" : thisWeekDistance, "lastWeek": lastWeekDistance, "image": "distance"  ] as [String : Any]
                
                let timeFeature3 = ["title" : "Time", "thisWeek" : thisWeekHours, "lastWeek": lastWeekHours, "image": "stopwatch"     ] as [String : Any]
                
                self.featureArray = [distanceFeature2 as! Dictionary<String, String>, kcalFeature1 as! Dictionary<String, String>, timeFeature3 as! Dictionary<String, String>]
                
                self.scrollView1.isPagingEnabled = true
                self.scrollView1.contentSize = CGSize(width: self.view.bounds.width * CGFloat(self.featureArray.count), height: 165)
                self.scrollView1.showsHorizontalScrollIndicator = false
                
                self.scrollView1.delegate = self
                self.loadFeatures()
                
                
                
                
            }) { (error) in
                print(error.localizedDescription)
            }

            
        }

        

    func loadFeatures () {
       
        for (index, feature) in featureArray.enumerated() {
            
            if let  featureView = Bundle.main.loadNibNamed("ActivitySummary1", owner: self, options: nil)?.first as? ActivitySummary1VC{
                featureView.actImage.image = UIImage(named: feature["image"]!)
                featureView.titleLable.text = feature["title"]
                featureView.thisWeek.text = feature["thisWeek"]
                featureView.lastWeek.text = feature["lastWeek"]
                
                scrollView1.addSubview(featureView)
                featureView.frame.size.width = self.view.bounds.size.width
                featureView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
            }
        }
    }
    
    
    //This function sums up all the data from the activity list and populates them in the container view
    func sumUpAllDistanceData (){
        
        //Clear the console
      
        
        /*ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
         for child in snapshot.childSnapshot(forPath: "activityList"){
         print(child.key)
         }
         
         */
        
        
        ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
             let value = snapshot.value as? NSDictionary
            let activityCountFromDb = value?["activityCountAsInt"] as? Int
            //print(activityCountFromDb ?? 0)
            //let activityCountFromDbAsCompleteInt = activityCountFromDb! as FIRDataSnapshot
          
           
        
            /* Continue here
            for _ in  activityCountFromDbAsCompleteInt{
                
                actListForLoop += 1
                //print("I am now \(myAge) years old")
            }
 */
          

            var actListForLoop: [AnyObject] = []
            
            for child in snapshot.childSnapshot(forPath: "activityList").children {
    
                let index = (child as! FIRDataSnapshot).key
                print(index)
                
     
                
                for child1 in snapshot.childSnapshot(forPath: "activityList/\(self.finalActListForLoop)").children{
                    
                    //Gives all the value
                  //print(child)
                  
                    
                    self.finalActListForLoopToAccessFinalDistance = (child1 as! FIRDataSnapshot).key
              print(self.finalActListForLoopToAccessFinalDistance)
                    
                }
   
                }
        
 
        })
    
        
        
        // Get number of activities and go through each list to extract data
        
     
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        pageIndicator.currentPage = Int(page)
    }

    @IBAction func debug(_ sender: Any) {
        sumUpAllDistanceData()
        
    }
}


