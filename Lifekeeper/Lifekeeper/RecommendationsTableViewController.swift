//
//  RecommendationsTableViewController.swift
//  Lifekeeper
//
//  Created by Max Winkler on 06/03/2017.
//  Copyright © 2017 Pristok. All rights reserved.
//

import UIKit

class RecommendationsTableViewController: UITableViewController {

    let list = ["Route", "Accesibility", "Diet"]
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell (style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = list[indexPath.row]
        return cell
    
    }
    
    }
    

