//
//  SettingsVC.swift
//  Lifekeeper
//
//  Created by Max Winkler on 10/10/2016.
//  Copyright © 2016 Pristok. All rights reserved.
//

import UIKit
import Firebase


class SettingsVC: UIViewController {
    //Declares outlets
    @IBOutlet weak var birthLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var langSegmented: UISegmentedControl!
    @IBOutlet weak var unitSegmented: UISegmentedControl!
    @IBOutlet weak var langLable: UILabel!
    @IBOutlet weak var unitLabel: UILabel!
    
    //Vars and lets
    var lang: String = ""
    var unitOfDistance: String = ""
    var ref: FIRDatabaseReference!
    
    let uid = (FIRAuth.auth()?.currentUser!.uid)! as String
    
    override func viewWillAppear(_ animated: Bool) {
        self.langLable.isHidden=true
        self.unitLabel.isHidden=true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        //Gets app version
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.versionLabel.text = "Version " + version
        }
        //Gets data from user
        ref = FIRDatabase.database().reference()
        ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let username = value?["fullName"] as? String ?? ""
            let birthday = value?["bday"] as? String ?? ""
            let gender = value?["gender"] as? String ?? ""
            let weight = value?["weight"] as! Double
            let height = value?["height"] as! Double
            let langValue = value?["lang"] as? String ?? ""
            let unitValue = value?["unit"] as? String ?? ""
      

       
            
            
            //Updates labels
            self.langLable.text = langValue
            if self.langLable.text == "English" {
                self.langSegmented.selectedSegmentIndex=0
            }else{
                self.langSegmented.selectedSegmentIndex=1
            }
            self.unitLabel.text = unitValue
            if self.unitLabel.text == "Km"{
                self.unitSegmented.selectedSegmentIndex=0
            }else{
                self.unitSegmented.selectedSegmentIndex=1
            }
            self.birthLabel.text = birthday
            self.weightLabel.text = ("\(weight)")
            self.heightLabel.text = ("\(height)")
            self.genderLabel.text = gender

        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    
    
    
    //Logout button
    @IBAction func logout(_ sender: Any) {
        try! FIRAuth.auth()!.signOut()
        var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        var vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "LoginView")
        
        self.present(vc, animated: true, completion: nil)
        
        
    }
    

    
    @IBAction func langSelector(_ sender: Any) {
        
        switch langSegmented.selectedSegmentIndex
        {
        case 0:
            lang = "English"
            var finalLangSelected = ref.child(byAppendingPath: "userData/\(uid)/lang")
            finalLangSelected.setValue(lang)
           
        case 1:
            lang  = "Spanish"
            var finalLangSelected = ref.child(byAppendingPath: "userData/\(uid)/lang")
            finalLangSelected.setValue(lang)
        default:
            break
        }
    }
    
    @IBAction func unitDistanceSelector(_ sender: Any) {
        switch unitSegmented.selectedSegmentIndex
        {
        case 0:
            unitOfDistance = "Km"
            var finalUnitSelected = ref.child(byAppendingPath: "userData/\(uid)/unit")
            finalUnitSelected.setValue(unitOfDistance)

            print(unitOfDistance)
        case 1:
            unitOfDistance  = "Mi"
            var finalUnitSelected = ref.child(byAppendingPath: "userData/\(uid)/unit")
            finalUnitSelected.setValue(unitOfDistance)
            print(unitOfDistance)
        default:
            break
        }
    }

        
    }
    


    
        
