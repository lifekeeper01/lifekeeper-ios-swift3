//
//  ShowFinalDataViewController.swift
//  Lifekeeper
//
//  Created by Max Winkler on 21/03/2017.
//  Copyright © 2017 Pristok. All rights reserved.
//

import UIKit
import Firebase

class ShowFinalDataViewController: UIViewController {

    var finalActListForLoop = 0
    var finalActListForLoopToAccessFinalDistance = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        
    }
    
    func loadData(){
        print("Hello World")
        
        var ref: FIRDatabaseReference!
        let uid = (FIRAuth.auth()?.currentUser!.uid)! as String
        
        ref = FIRDatabase.database().reference()
        ref.child("userData").child(uid).child("activityList").observeSingleEvent(of: .value, with: { (snapshot) in
            
            var actListForLoop: [AnyObject] = []
            
            for child in snapshot.childSnapshot(forPath: "activityList").children {
                
                let index = (child as! FIRDataSnapshot).key
                print(index)
                
                /* print("go now")
                 actListForLoop.append(child as! Int)
                 print(actListForLoop[1])
                 */
                
                // This loop iterates over number of activities. The result is each number of activities from 1 to n
                for i in index.characters{
                    actListForLoop.append(i as AnyObject)
                    self.finalActListForLoop = actListForLoop.count
                    print(self.finalActListForLoop)
                    
                }
                
                
                
                for child1 in snapshot.childSnapshot(forPath: "activityList/\(self.finalActListForLoop)").children{
                    
                    //Gives all the value
                    print(child)
                    self.finalActListForLoopToAccessFinalDistance = (child1 as! FIRDataSnapshot).key
                    print(self.finalActListForLoopToAccessFinalDistance)
                    
                }}})}
}


