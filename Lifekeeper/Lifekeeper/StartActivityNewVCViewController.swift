//
//  StartActivityNewVCViewController.swift
//  Lifekeeper
//
//  Created by Apple on 9/2/17.
//  Copyright © 2017 Pristok. All rights reserved.
//
import UIKit
import CoreLocation
import MapKit
import HealthKit
import Firebase

class StartActivityNewVCViewController: UIViewController,CLLocationManagerDelegate, MKMapViewDelegate {
    
    var locManager: CLLocationManager = CLLocationManager()
    
    @IBOutlet weak var currentSpeed: UILabel!
    @IBOutlet weak var currentDistance: UILabel!
    @IBOutlet weak var avgSpeedLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var pauselbl: UIButton!
    @IBOutlet weak var resumeLbl: UIButton!
    @IBOutlet weak var stopLbl: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var kcalBurnedLbl: UILabel!
    

    
    var finalTimeEllapsed: String = ""
    var myTimer: Timer? = nil
    var counter = 0
    var distace = 0.0
    var locations = [CLLocation]()

    var speed: CLLocationSpeed! = CLLocationSpeed()
    var startLocation:CLLocation!
    var lastLocation: CLLocation!
    var traveledDistance:Double = 0
    var sec = 00
    var min = 00
    var hour = 00
    var finalSeconds: Double = 0
    var avgSpeed: Double = 0.0
    var speedKM: CLLocationSpeed = CLLocationSpeed()
    var finalDistanceMs = 0.0
    var finalDistance = 0.0
    var ref:FIRDatabaseReference!
    var kcalBurned = 0.0
    var activityThisWeek =  0
    var activityThisWeekString = ""
    let uid = (FIRAuth.auth()?.currentUser!.uid)! as String
    var activityCountNumberInt = ""
    var activiyCountNumberString = ""
    var activityCountInt: Int = 0
    var thisWeekActivity: Int = 0
    
    
    var totalDistanceDb: Double = 0.0
    var totalKcalDb: Double = 0.0
    var totalHoursDb: Double = 0.0
    let mets = 3.8
    var kg: Double = 0.0
    
    //More location managers
    

       override func viewWillAppear(_ animated: Bool) {
      
        
        resumeLbl.isHidden = true
        pauselbl.isHidden = false
        
        ref = FIRDatabase.database().reference()
        ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            self.kg = value?["weight"] as! Double
            print(self.kg)

        })
       
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)


        currentSpeed.text = String("0.0")
        //Ask for permission GPS
        locManager = CLLocationManager()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestAlwaysAuthorization()
        locManager.startUpdatingLocation()
        mapView.showsUserLocation = true
        
   
    }
    //end viewDidLoad()
    

    
    
            
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        //Get user location
        
    

        if startLocation == nil {
            startLocation = locations.first
        } else {
            if let lastLocation = locations.last {
                let distance = startLocation.distance(from: lastLocation)
                let lastDistance = lastLocation.distance(from: lastLocation)
                traveledDistance += lastDistance
                

             
        
                
                /*
                print( "\(startLocation)")
                print( "\(lastLocation)")
                print("FULL DISTANCE: \(traveledDistance)km")
                print("STRAIGHT DISTANCE: \(distance) ")
           */
                
                //adapt to km
               
                finalDistanceMs = distance
                finalDistance = round(distance) / 1000
                currentDistance.text = String("\(finalDistance) km")

                
            }
        }
        lastLocation = locations.last
    }
    //func that updates content every second
    
    func update() {
        //Update time label and add 1
        
              sec += 1
        finalSeconds += 1
        
        if sec > 59{
            min += 1
            sec = 0
            hour = 0
        }
        if min > 59{
            hour += 1
            min = 0
            sec = 0
        }

        finalTimeEllapsed = String(("\((hour)):\((min)):\((sec))"))
            timeLbl.text = finalTimeEllapsed

        //SPEED
        
       
            speed = locManager.location?.speed
 
        speedKM = (speed * 3.6)
        
        avgSpeed = round((finalDistanceMs / finalSeconds) * 3.6)
   
        
        let finalSpeed = (round(speedKM))
        if finalSpeed == -4 {
            print("Negative speed")
        }else{
            currentSpeed.text = ("\(finalSpeed) km/h")
            avgSpeedLbl.text = "\(avgSpeed) km/h"
            
        }
 
 
        
        //Calculate the kcal burned during activity
        
        //Kcal = Mets (3,5) * kg * hours
        
        if finalDistance == 0  || finalSeconds == 0 {
            kcalBurnedLbl.text = "0 kcal"
            
        }else{
            
            kcalBurned = mets * kg * (finalSeconds/60/60)
            print(kcalBurned)
            kcalBurnedLbl.text = ("\(kcalBurned) Kcal")
        }
    }
    
    //this function saves user data and writes it to healthkit
    
    func saveData(){

        ref = FIRDatabase.database().reference()
        myTimer?.invalidate()
        locManager.stopUpdatingLocation()

        //Gets data from user and compare
     
        ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
          
            let value = snapshot.value as? NSDictionary
            self.kg = value?["weight"] as! Double
            print(self.kg)
  
            self.activityCountInt = value?["activityCountAsInt"] as! Int
        

            if self.activityCountInt >= 0 {
               
                self.activityCountInt = self.activityCountInt + 1

                 let activityCountIntForDb = self.ref.child(byAppendingPath: "userData/\(self.uid)/activityCountAsInt")
                activityCountIntForDb.setValue(self.activityCountInt)

                print(self.activityCountInt)
                print("Ok")
               
                //Save user data
                self.ref?.child("userData").child(self.uid).child("activityList").child("\(self.activityCountInt)").setValue([
                    
                    "totalDistance" : self.finalDistance as AnyObject,
                    "totalDuration" : self.finalSeconds as AnyObject,
                    "totalKcal" : self.kcalBurned as AnyObject,
                    "avgSpeed" : self.avgSpeed as AnyObject
                    ])

                           }
        
        }) { (error) in
            print(error.localizedDescription)
            
        }
 
        // Save time in seconds, distance in km, kcal burned, avg speed in km/h+
        //Declare final let
        // add one to activity´s completed this week
          // Get user value for activityCount
        
        ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
          
            let value = snapshot.value as? NSDictionary
           // let activityCountForBd = value?["totalActivity"] as! Int
            
            //print(activityCountForBd)
            //print("Above is activityCount in parent node")

            let activityCountForBdFinal = self.ref.child("userData/\(self.uid)/totalActivity")
            activityCountForBdFinal.setValue("\(self.activityCountInt)")
            print("All good")

            
        }) { (error) in
            print(error.localizedDescription)
        }
        updateMainData()
    }

    //This func updates the total values (totalDistance, totalKcal, totalHours)
    func updateMainData() {
        
        ref = FIRDatabase.database().reference()
        //Gets data from user and compare
        ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
             self.totalDistanceDb = value?["totalDistance"] as! Double
             self.totalKcalDb = Double(value?["totalKcal"] as! Int)
             self.totalHoursDb = Double(value?["totalHours"] as! Double)

            print(self.totalDistanceDb,self.totalKcalDb,self.totalHoursDb)
            
            //Save user data
           
            /*self.ref?.child("userData").child(self.uid).child("activityList").child("\(self.activityCountInt)").setValue([
                
                "totalDistance" : self.finalDistance as AnyObject,
                "totalDuration" : self.finalSeconds as AnyObject,
                "totalKcal" : self.kcalBurned as AnyObject,
                "avgSpeed" : self.avgSpeed as AnyObject
                
                ])
 */

//Make sure to upadate data
            if self.totalDistanceDb >= 0 {
                
                self.totalDistanceDb = self.totalDistanceDb + self.finalDistance
                let finalTotalDistanceDb = self.ref.child(byAppendingPath: "userData/\(self.uid)/totalDistance")
                finalTotalDistanceDb.setValue(self.totalDistanceDb)
   
            }
       
            if self.totalKcalDb >= 0 {
                
                self.totalKcalDb = self.totalKcalDb + self.kcalBurned
                let finalTotalKcalDb = self.ref.child(byAppendingPath: "userData/\(self.uid)/totalKcal")
                finalTotalKcalDb.setValue(self.totalKcalDb)
  
            }
            
            if self.totalHoursDb >= 0 {
                self.totalHoursDb = self.totalHoursDb + self.finalSeconds/60/60
                let finalTotalHoursDb = self.ref.child(byAppendingPath: "userData/\(self.uid)/totalHours")
                finalTotalHoursDb.setValue(self.totalHoursDb)
       
            }

        })
        

    }

    @IBAction func pauseResume(_ sender: Any) {
        
        myTimer?.invalidate()
        pauselbl.isHidden = true
        resumeLbl.isHidden = false
        locManager.stopUpdatingLocation()
        
    }
    @IBAction func resumeBtn(_ sender: Any) {
        pauselbl.isHidden = false
        resumeLbl.isHidden = true
      myTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        locManager.startUpdatingLocation()
    }
  
    @IBAction func stopBtn(_ sender: Any) {
        
        myTimer?.invalidate()
        let refreshAlert = UIAlertController(title: "Save activity", message: "Continue to save your data", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
           self.saveData()
            
                     //Go to another view controller
            var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            var vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "activityCompleted")
            
            self.present(vc, animated: true, completion: nil)

        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
       refreshAlert.dismiss(animated: true, completion: nil)
            self.myTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
}


