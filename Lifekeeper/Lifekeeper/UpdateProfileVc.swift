//
//  UpdateProfileVc.swift
//  Lifekeeper
//
//  Created by Max Winkler on 04/03/2017.
//  Copyright © 2017 Pristok. All rights reserved.
//

import UIKit
import Firebase

class UpdateProfileVc: UIViewController, UITextFieldDelegate{

    //Outlets
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var bday: UIDatePicker!
    @IBOutlet weak var gender: UISegmentedControl!
    
    
    
    //Vars and lets
    var ref:FIRDatabaseReference!
     let uid = (FIRAuth.auth()?.currentUser!.uid)! as String
    var dDay: Int = 0
    var dMonth: Int = 0
    var dYear: Int = 0
    var completeDate: String = ""
    var genderValue: String = ""
    
    
    
    //Date changed
    func dateChanged(_ sender: UIDatePicker) {
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        
        if let day = componenets.day, let month = componenets.month, let year = componenets.year  {
            completeDate = "\(String(day)  + "/") \(String(month) + "/") \(String(year))"
            dDay = day
            dMonth = month
            dYear = year
            
            

        }
        
    }

    //View did load
    override func viewDidLoad() {
        super.viewDidLoad()
        bday.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
       self.name.delegate = self
    

        
        //Gets data from user
        ref = FIRDatabase.database().reference()
        ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let username = value?["fullName"] as? String ?? ""
            //let birthday = value?["bday"] as? String ?? ""
            //let gender = value?["gender"] as? String ?? ""
            let weight = value?["weight"] as! Double
            let height = value?["height"] as! Double
            let email = value?["email"] as? String ?? ""

            //Updates labels
            if self.genderValue == "Male" {
                self.gender.selectedSegmentIndex = 0
            }else{
                self.gender.selectedSegmentIndex  = 1
            }
            self.name.text = username
            self.height.text = ("\(height)")
            self.weight.text = ("\(weight)")
            self.email.text = email
       
            //NOT WORKING
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/mm/yyyy"
            if let convertedStartDate = dateFormatter.date(  from: "\(self.dDay)/\(self.dMonth)/\(self.dYear)" ) {
                print(convertedStartDate)
                self.bday.setDate(convertedStartDate, animated: true)
                
            }
            
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    
    //End editing on touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func genderPicker(_ sender: Any) {
        
        switch gender.selectedSegmentIndex
        {
        case 0:
            genderValue = "Male"
            var finalGenderValue = ref?.child(byAppendingPath: "userData/\(uid)/gender")
            finalGenderValue?.setValue(genderValue)
          
        case 1:
            genderValue = "Female"
            var finalGenderValue = ref?.child(byAppendingPath: "userData/\(uid)/gender")
            finalGenderValue?.setValue(genderValue)
            
            
        default:
            break
        }
        
    }
    @IBAction func updateAction(_ sender: Any) {
       
        

        
       

        //Declare location for data and update it
        
        let finalName = ref.child(byAppendingPath: "userData/\(uid)/fullName")
        finalName.setValue(name.text)
        
        let finalHeight = ref.child(byAppendingPath: "userData/\(uid)/height")
        finalHeight.setValue(height.text)
        
        let finalWeight = ref.child(byAppendingPath: "userData/\(uid)/weight")
        finalWeight.setValue(weight.text)
        
        let finalBday = ref.child(byAppendingPath: "userData/\(uid)/bday")
        finalBday.setValue(completeDate)

        let alertController = UIAlertController(title: "Modification complete", message:
            "Information will be updated upon restarting the app", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

  }
