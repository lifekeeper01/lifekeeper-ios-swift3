//
//  ViewController.swift
//  Lifekeeper
//
//  Created by Max Winkler on 27/09/2016.
//  Copyright © 2016 Pristok. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController, UITextFieldDelegate {
    
   

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var loginLBL: UIButton!
    @IBOutlet weak var createAccountLbl: UIButton!
    @IBOutlet weak var imageViewCorrectIncorrect: UIImageView!
    @IBOutlet weak var imageViewCorrectIncorrect1: UIImageView!


    override func viewDidLoad() {
        
        self.email.delegate = self
        self.password.delegate  = self
        
        super.viewDidLoad()
     
       // try! FIRAuth.auth()!.signOut()
        
        // Do any additional setup after loading the view, typically from a nib.
        
     /*   Trying to add text field validation
         
         let tick: UIImage = UIImage(named:"error.png")!
        let cross: UIImage = UIImage(named: "success.png")!
       
        while (email.text?.characters.count)! >= 5 { // Output cross
           imageViewCorrectIncorrect.image = UIImage(named: "error.png")
            
         
        
        } */
        
    
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil && self.email.text != FIRAuth.auth()?.currentUser?.email as String!{
                print("User in")
                
                var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                var vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVc")
                
                self.present(vc, animated: true, completion: nil)

                
            } else {
                print("user NOT in")            }
        }
        
        
        loginLBL.layer.cornerRadius = 15
        createAccountLbl.layer.cornerRadius = 15
        
        if (FIRAuth.auth()?.currentUser) != nil {
            let userMAIL = FIRAuth.auth()?.currentUser?.email
            
                     self.username.text = userMAIL
            
            
            
        }else{

            self.username.text = ""
        }

        

        
      
        
    }
    
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
            }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func createAccount(_ sender: AnyObject) {
        
        self.email.resignFirstResponder()
        self.password.resignFirstResponder()
        
        if self.email.text == "" ||  self.password.text == "" {
            
            let alertController = UIAlertController(title: "Error", message:
                "Looks like the fields are empty", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)

        }else {
           
            FIRAuth.auth()?.createUser(withEmail: email.text!, password: password.text!) { (user, error) in
                // ...
                
                if error != nil {
                    print("Incorrect sign up")
                    print(error?.localizedDescription)
                    let alertController = UIAlertController(title: "You have not signed up", message:
                        "Your account has NOT been created", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    print("Account created")
                   
                       var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                     
                     var vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "moreInfo") 
                     
                     self.present(vc, animated: true, completion: nil)

                }
                
            }

            
            
        }
        
          }
 
    
    
    @IBAction func login(_ sender: AnyObject) {
        
        self.email.resignFirstResponder()
        self.password.resignFirstResponder()

        if self.email.text == "" ||  self.password.text == "" {
            
            let alertController = UIAlertController(title: "Error", message:
                "Looks like the fields are empty", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
                
        }else{
            FIRAuth.auth()?.signIn(withEmail: email.text!, password: password.text!) { (user, error) in
                // ...
                var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                var vc: UITabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarVc") as! UITabBarController
                
                self.present(vc, animated: true, completion: nil) 
                
                if error != nil{
                    print(error?.localizedDescription)
                    print("Incorrect")
                    let alertController = UIAlertController(title: "Login Unsuccesful", message:
                        "You have NOT logged in", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)

                }else{
                   
                    
                    self.username.text = "You have logged in as: \(user?.email)"
/*
                    let alertController = UIAlertController(title: "Logged In", message:
                        "You have logged in", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    
                   */
                 
                    
                    
                  
             
                    
                    
                    
                                   }
                
            }
            
            
        }
        
      }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
   }







