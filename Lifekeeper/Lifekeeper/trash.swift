//
//  trash.swift
//  Lifekeeper
//
//  Created by Max Winkler on 13/10/2016.
//  Copyright © 2016 Pristok. All rights reserved.
//

/*
@IBOutlet weak var nameSurname:  UITextField!
@IBOutlet weak var height: UITextField!
@IBOutlet weak var weight: UITextField!
@IBOutlet weak var progressBar: UIProgressView!
@IBOutlet weak var dateLabel: UILabel!
@IBOutlet weak var datePicker: UIDatePicker!

var ref: FIRDatabaseReference!
let userEmail = FIRAuth.auth()?.currentUser?.email
let userUid = FIRAuth.auth()?.currentUser?.uid

override func viewDidLoad() {
    super.viewDidLoad()
    dateFunc()
    ref = FIRDatabase.database().reference()
    //Initializing DB
    
    
    
    
    //Text field delegates
    nameSurname.delegate = self
    height.delegate = self
    weight.delegate = self
    
    self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard")))
    
}

func dismissKeyboard() {
    
    nameSurname.resignFirstResponder()
    height.resignFirstResponder()
    weight.resignFirstResponder()
}

func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    nameSurname.resignFirstResponder()
    height.resignFirstResponder()
    weight.resignFirstResponder()
    return true
}


//function to store data

func postUserData() {
    
    self.ref.child("userData").child(userUid!).setValue( ["userEmail" : userEmail,
                                                          "userName" : nameSurname.text,
                                                          "userHeight" : height.text,
                                                          "userWeight" : weight.text,
                                                          "userDOB" : dateLabel
        
        
        ])
    
    
    
    
    
}



@IBAction func saveContinueButton(_ sender: AnyObject) {
    
    let user = FIRAuth.auth()?.currentUser
    if let user = user {
        let changeRequest = user.profileChangeRequest()
        
        changeRequest.displayName = nameSurname.text
        
        changeRequest.commitChanges { error in
            if error != nil {
                // An error happened.
                print("Error")
            } else {
                // Profile updated.
                print("OK")
            }
            
            
            
        }
        
        postUserData()
    }
}

func dateFunc () {
    
    let formatter = DateFormatter()
    formatter.dateFormat = "MMM dd, YYY"
    dateLabel.text = formatter.string(from: datePicker.date)
    
    
}

@IBAction func pickerValueChanged(_ sender: AnyObject) {
    
    dateFunc()
    
    
}


}


*/
