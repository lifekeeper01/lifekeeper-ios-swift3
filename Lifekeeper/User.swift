//
//  User.swift
//  Lifekeeper
//
//  Created by Max Winkler on 01/03/2017.
//  Copyright © 2017 Pristok. All rights reserved.
//

///App Guidelines

//1. Labels must be 20
//2. Buttons must be 24 and yellow semibold
//3. Sub labels ust be 16


/*
 
 Me vars and lets
 
 var ref: FIRDatabaseReference!
 let uid = (FIRAuth.auth()?.currentUser!.uid)! as String
 
 ref = FIRDatabase.database().reference()
 ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
 // Get user value
 let value = snapshot.value as? NSDictionary
 let username = value!["fullName"] as? String ?? ""
 
 let totalDistance = value?["totalDistance"] as? String ?? ""
 let totalActivity = value?["totalActivity"] as? String ?? ""
 let totalKcal = value?["totalKcal"] as? String ?? ""
 let totalHours = value?["totalHours"] as? String ?? ""
 
 let lastWeekDistance = value?["lastWeekDistance"] as? String ?? ""
 let lastWeekActivity = value?["lastWeekActivity"] as? String ?? ""
 let lastWeekKcal = value?["lastWeekKcal"] as? String ?? ""
 let lastWeekHours = value?["lastWeekHours"] as? String ?? ""
 
 let thisWeekDistance = value?["thisWeekDistance"] as? String ?? ""
 let thisWeekActivity = value?["thisWeekActivity"] as? String ?? ""
 let thisWeekKcal = value?["thisWeekKcal"] as? String ?? ""
 let thisWeekHours = value?["thisWeekHours"] as? String ?? ""
 
 self.allTimeDistance.text = String(describing: totalDistance)
 self.allTimeTime.text = String(describing: totalHours)
 self.allTimeKcal.text = String(describing: totalKcal)
 
 
 
 
 
 
 
 
 
 
 //
 
 //  StartActivityNewVCViewController.swift
 
 //  Lifekeeper
 
 //
 
 //  Created by Apple on 9/2/17.
 
 //  Copyright © 2017 Pristok. All rights reserved.
 
 //
 
 
 
 +
 
 import UIKit
 
 import CoreLocation
 
 +import MapKit
 
 
 
 -class StartActivityNewVCViewController: UIViewController, CLLocationManagerDelegate {
 
 -
 
 -
 
 +class StartActivityNewVCViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
 
 @IBOutlet weak var mapView: MKMapView!
 
 -    var locationManager: CLLocationManager?
 
 +
 
 +    let locationManager = CLLocationManager()
 
 
 
 override func viewDidLoad() {
 
 super.viewDidLoad()
 
 
 
 -        locationManager = CLLocationManager()
 
 -        locationManager!.delegate = self
 
 -
 
 -        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
 
 -            locationManager!.startUpdatingLocation()
 
 -        } else {
 
 -            locationManager!.requestWhenInUseAuthorization()
 
 -        }
 
 +        self.locationManager.delegate = self
 
 +        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
 
 +        self.locationManager.requestWhenInUseAuthorization()
 
 +        self.locationManager.startUpdatingLocation()
 
 +        self.mapView.showsUserLocation = true
 
 }
 
 
 
 -    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
 
 +    override func didReceiveMemoryWarning() {
 
 +        super.didReceiveMemoryWarning()
 
 +            }
 
 +
 
 +    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
 
 +        let location = locations.last
 
 
 
 +        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
 
 +        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
 
 +        self.mapView.setRegion(region, animated: true)
 
 +        self.locationManager.stopUpdatingLocation()
 
 
 
 -        switch status {
 
 -        case .notDetermined:
 
 -            print("NotDetermined")
 
 -        case .restricted:
 
 -            print("Restricted")
 
 -        case .denied:
 
 -            print("Denied")
 
 -        case .authorizedAlways:
 
 -            print("AuthorizedAlways")
 
 -        case .authorizedWhenInUse:
 
 -            print("AuthorizedWhenInUse")
 
 -            locationManager!.startUpdatingLocation()
 
 -        }
 
 }
 
 
 
 -    private func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
 
 -
 
 -        let location = locations.first!
 
 -        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500)
 
 -        mapView.setRegion(coordinateRegion, animated: true)
 
 -        locationManager?.stopUpdatingLocation()
 
 -        locationManager = nil
 
 +    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
 
 +        print("Errors" + error.localizedDescription)
 
 }
 
 
 
 -    func locationManager(manager: CLLocationManager, didFailWithError error: Error) {
 
 -        print("Failed to initialize GPS: ", error.description)
 
 -    }
 
 +    
 
 +    
 
 +    
 
 +    
 
 +    
 
 +    
 
 }
 
 
 
 
 /*
 
 //
 //  MeVC.swift
 //  Lifekeeper
 //
 //  Created by Max Winkler on 10/10/2016.
 //  Copyright © 2016 Pristok. All rights reserved.
 //
 
 import UIKit
 import Firebase
 
 class MeVC: UIViewController, UIScrollViewDelegate{
 
 
 //Outlets
 
 @IBOutlet weak var nameLbl: UILabel!
 @IBOutlet weak var scrollView1: UIScrollView!
 @IBOutlet weak var pageIndicator: UIPageControl!
 @IBOutlet weak var allTimeDistance: UILabel!
 @IBOutlet weak var allTimeTime: UILabel!
 @IBOutlet weak var allTimeKcal: UILabel!
 
 var featureArray = [Dictionary<String,String> ]()
 var ref: FIRDatabaseReference!
 let uid = (FIRAuth.auth()?.currentUser!.uid)! as String
 var finalActListForLoop = 0
 var finalActListForLoopToAccessFinalDistance = ""
 
 //Vars and lets
 
 
 override func viewDidLoad() {
 super.viewDidLoad()
 
 
 FIRAuth.auth()?.addStateDidChangeListener { auth, user in
 if user != nil {
 
 }}
 
 
 
 
 ref = FIRDatabase.database().reference()
 ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
 // Get user value
 let value = snapshot.value as? NSDictionary
 let username = value!["fullName"] as? String ?? ""
 
 let totalDistance = value?["totalDistance"] as? String ?? ""
 let totalActivity = value?["totalActivity"] as? String ?? ""
 let totalKcal = value?["totalKcal"] as? String ?? ""
 let totalHours = value?["totalHours"] as? String ?? ""
 
 let lastWeekDistance = value?["lastWeekDistance"] as? String ?? ""
 let lastWeekActivity = value?["lastWeekActivity"] as? String ?? ""
 let lastWeekKcal = value?["lastWeekKcal"] as? String ?? ""
 let lastWeekHours = value?["lastWeekHours"] as? String ?? ""
 
 let thisWeekDistance = value?["thisWeekDistance"] as? String ?? ""
 let thisWeekActivity = value?["thisWeekActivity"] as? String ?? ""
 let thisWeekKcal = value?["thisWeekKcal"] as? String ?? ""
 let thisWeekHours = value?["thisWeekHours"] as? String ?? ""
 
 self.allTimeDistance.text = String(describing: totalDistance)
 self.allTimeTime.text = String(describing: totalHours)
 self.allTimeKcal.text = String(describing: totalKcal)
 
 
 
 
 
 
 //var pageViewController: UIPageViewController!
 //var pageTitles: NSArray!
 
 let kcalFeature1 = ["title" : "Kilocalories", "thisWeek" : thisWeekKcal, "lastWeek": lastWeekKcal, "image": "fire"    ] as [String : Any]
 let distanceFeature2 = ["title" : "Distance", "thisWeek" : thisWeekDistance, "lastWeek": lastWeekDistance, "image": "distance"  ] as [String : Any]
 
 let timeFeature3 = ["title" : "Time", "thisWeek" : thisWeekHours, "lastWeek": lastWeekHours, "image": "stopwatch"     ] as [String : Any]
 
 self.featureArray = [distanceFeature2 as! Dictionary<String, String>, kcalFeature1 as! Dictionary<String, String>, timeFeature3 as! Dictionary<String, String>]
 
 self.scrollView1.isPagingEnabled = true
 self.scrollView1.contentSize = CGSize(width: self.view.bounds.width * CGFloat(self.featureArray.count), height: 165)
 self.scrollView1.showsHorizontalScrollIndicator = false
 
 self.scrollView1.delegate = self
 self.loadFeatures()
 
 
 self.nameLbl.text = username
 
 }) { (error) in
 print(error.localizedDescription)
 }
 
 
 
 }
 func loadFeatures () {
 
 for (index, feature) in featureArray.enumerated() {
 
 if let  featureView = Bundle.main.loadNibNamed("ActivitySummary1", owner: self, options: nil)?.first as? ActivitySummary1VC{
 
 featureView.actImage.image = UIImage(named: feature["image"]!)
 featureView.titleLable.text = feature["title"]
 featureView.thisWeek.text = feature["thisWeek"]
 featureView.lastWeek.text = feature["lastWeek"]
 
 scrollView1.addSubview(featureView)
 featureView.frame.size.width = self.view.bounds.size.width
 featureView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
 
 }
 }
 }
 
 
 //This function sums up all the data from the activity list and populates them in the container view
 func sumUpAllDistanceData (){
 
 //Clear the console
 
 
 /*ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
 for child in snapshot.childSnapshot(forPath: "activityList"){
 print(child.key)
 }
 
 */
 
 
 ref.child("userData").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
 let value = snapshot.value as? NSDictionary
 let activityCountFromDb = value?["activityCountAsInt"] as? Int
 //print(activityCountFromDb ?? 0)
 //let activityCountFromDbAsCompleteInt = activityCountFromDb! as FIRDataSnapshot
 
 
 
 
 /* Continue here
 for _ in  activityCountFromDbAsCompleteInt{
 
 actListForLoop += 1
 //print("I am now \(myAge) years old")
 }
 */
 
 
 var actListForLoop: [AnyObject] = []
 
 for child in snapshot.childSnapshot(forPath: "activityList").children {
 
 let index = (child as! FIRDataSnapshot).key
 print(index)
 
 
 
 for child1 in snapshot.childSnapshot(forPath: "activityList/\(self.finalActListForLoop)").children{
 
 //Gives all the value
 //print(child)
 
 
 self.finalActListForLoopToAccessFinalDistance = (child1 as! FIRDataSnapshot).key
 print(self.finalActListForLoopToAccessFinalDistance)
 
 }
 
 }
 
 
 })
 
 
 
 // Get number of activities and go through each list to extract data
 
 
 }
 
 func scrollViewDidScroll(_ scrollView: UIScrollView) {
 let page = scrollView.contentOffset.x / scrollView.frame.size.width
 pageIndicator.currentPage = Int(page)
 }
 
 @IBAction func debug(_ sender: Any) {
 sumUpAllDistanceData()
 
 }
 }
 
 
 
 */
 
 */
